from flask import Flask
from flask import request
from flask import Response
import uuid
import random
import requests

MAIN_SERVER = "https://jetbra.in"
MANDATORY_ARGS = ["buildNumber", "clientVersion", "hostName", "machineId", "productCode", "productFamilyId", "salt", "secure", "userName"]
USER_NAMES = ["Marchelle","Flo","Mitchell","Orlando","Lisabeth","Isreal","Lynelle","Cassi","Eryn","Fleta","Curt","Lurlene","Pearlene","Pierre","Jalisa","Tu","Donald","Francie","Vanda","Dyan","Mitchell","Kizzie","Mana","Lilliam","Connie","Voncile","Camila","Arthur","Branda","Danuta", "Sophia", "Sarah", "Joe", "George"]
HOST_NAMES = ["Home", "HomeComputer", "Notebook", "Laptop", "PC", "MyPC", "Computer"]

app = Flask(__name__)

def verify_args(args):
    if len(args) < len(MANDATORY_ARGS):
        return False

    return all(item in args.keys() for item in MANDATORY_ARGS)


def pick_random_host():
    return random.choice(HOST_NAMES)

def pick_random_name():
    return random.choice(USER_NAMES)

def scramble_fields(args):
    args["machineId"] = uuid.uuid4()
    args["hostName"] = pick_random_host()
    args["userName"] = pick_random_name()

    return args



@app.route("/rpc/releaseTicket.action", methods=['GET'])
@app.route("/rpc/prolongTicket.action", methods=['GET'])
@app.route("/rpc/obtainTicket.action",  methods=['GET'])
def ticket_handler():
    if not verify_args(request.args):
        return "ERROR"

    new_args = scramble_fields(request.args.to_dict(flat=True))

    headers = {'user-agent': 'Java/11.0.15', 'accept': 'text/html, image/gif, image/jpeg, *; q=.2, */*, q=.2'}
    r = requests.get(MAIN_SERVER + request.path, headers=headers, params=new_args)

    return Response(r.text, mimetype='text/xml')